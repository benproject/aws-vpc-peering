
output "vpc1_addr_private" {
  description = "The Private IP of the VPC 1"
  value = aws_instance.vpc1.private_ip
}

output "vpc1_addr_public" {
  description = "The Public IP of the VPC 1"
  value = aws_instance.vpc1.public_ip
}


output "vpc2_addr_private" {
  description = "The Private IP of the VPC 2"
  value = aws_instance.vpc2.private_ip
}


output "vpc2_addr_public" {
  description = "The Public IP of the VPC 2"
  value = aws_instance.vpc2.public_ip
}