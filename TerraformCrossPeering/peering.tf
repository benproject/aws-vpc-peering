// CREATE PEERING BETWEEN VPC1 AND VPC2

resource "aws_vpc_peering_connection" "vpc_peering" {
  provider    = aws.vpc1
  peer_vpc_id = aws_vpc.vpc2.id
  vpc_id      = aws_vpc.vpc1.id
  peer_region = var.VPC_2_REGION

  tags = {
    Name = "VPC Peering VPC1 and VPC2"
  }
}

resource "aws_vpc_peering_connection_accepter" "peering-accepter" {
  provider                  = aws.vpc2
  vpc_peering_connection_id = aws_vpc_peering_connection.vpc_peering.id
  auto_accept               = true
}

