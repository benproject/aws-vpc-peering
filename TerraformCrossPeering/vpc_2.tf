// CREATE VPC
resource "aws_vpc" "vpc2" {
  provider             = aws.vpc2
  cidr_block           = var.VPC_2_SUBNET
  enable_dns_hostnames = "true"
  enable_dns_support   = "true"

  tags = {
    Name = var.VPC_2_NAME
  }
}

// CREATE GATEWAY
resource "aws_internet_gateway" "vpc2" {
  provider = aws.vpc2
  vpc_id   = aws_vpc.vpc2.id

  tags = {
    Name = var.VPC_2_NAME
  }
}

// CREATE ROUTE TABLE
resource "aws_route_table" "vpc2" {
  provider = aws.vpc2
  vpc_id   = aws_vpc.vpc2.id
  route {
    cidr_block = "0.0.0.0/0"
    gateway_id = aws_internet_gateway.vpc2.id
  }

  route {
    cidr_block = var.VPC_1_SUBNET
    gateway_id = aws_vpc_peering_connection.vpc_peering.id
  }

  tags = {
    Name = var.VPC_2_NAME
  }
}

// CREATE SUBNET
resource "aws_subnet" "vpc2" {
  provider   = aws.vpc2
  vpc_id     = aws_vpc.vpc2.id
  cidr_block = var.VPC_2_SUBNET

  map_public_ip_on_launch = "true"

  tags = {
    Name = var.VPC_2_NAME
  }
}

resource "aws_route_table_association" "vpc2" {
  provider       = aws.vpc2
  subnet_id      = aws_subnet.vpc2.id
  route_table_id = aws_route_table.vpc2.id
}

// CREATE SECURITY GROUP
resource "aws_security_group" "vpc2" {
  provider = aws.vpc2
  vpc_id   = aws_vpc.vpc2.id

  ingress {
    from_port   = 22
    to_port     = 22
    protocol    = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
    description = "Allow input SSH"
  }

  ingress {
    from_port   = 0
    to_port     = 0
    protocol    = "-1"
    cidr_blocks = [var.VPC_1_SUBNET]
    description = "Allow all input traffic from other VPC"
  }

  egress {
    from_port   = 0
    to_port     = 0
    protocol    = "-1"
    cidr_blocks = ["0.0.0.0/0"]
    description = "Allow all ouput traffic from other VPC"
  }

  tags = {
    Name        = var.VPC_2_NAME
    Description = var.VPC_2_NAME
  }
}

// CREATE INSTANCE
resource "aws_instance" "vpc2" {
  provider = aws.vpc2

  ami                         = var.AMI_VPC_2
  instance_type               = var.INSTANCE_TYPE

  key_name                    = var.KEY_PAIR_NAME
  vpc_security_group_ids      = [aws_security_group.vpc2.id]
  subnet_id                   = aws_subnet.vpc2.id
  associate_public_ip_address = true
  source_dest_check           = false

  tags = {
    Name = var.VPC_2_NAME
  }
}
