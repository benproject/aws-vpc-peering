///////////////// GLOBAL VARIABLE //////////////////////

variable "INSTANCE_TYPE" {
  default = "t2.micro"
}

variable "KEY_PAIR_NAME" {
 // default     = "keyPairEC2"
  description = "Keypair Name used to connect to EC2 Instances"
}

variable "KEY_PAIR_PATH" {
 // default     = "keyPairEC2.pem"
  description = "Keypath used connect to EC2 Instances via ssh"
}

//////////////// FIRST VPC /////////////////

variable "VPC_1_REGION" {
  default = "eu-west-1"
}

variable "VPC_1_NAME" {
  default = "vpc-1-name"
}

# variable "VPC_1_KEY_INSTANCE" {
#   default = "keyPairEC2"
#   description = "Keypair to use to connect to EC2 Instances"
# }

variable "VPC_1_SUBNET" {
  default = "192.168.1.0/24"
}

variable "AMI_VPC_1" {
  default = "ami-09693313102a30b2c"
}
//////////////// SECOND VPC /////////////////

variable "VPC_2_REGION" {
  default = "eu-west-1"
}

variable "VPC_2_NAME" {
  default = "vpc-2-name"
}

# variable "VPC_2_KEY_INSTANCE" {
#   default = "keyPairEC2"
# }

variable "VPC_2_SUBNET" {
  default = "192.168.2.0/24"
}

variable "AMI_VPC_2" {
  default = "ami-09693313102a30b2c"
}
