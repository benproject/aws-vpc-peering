resource "null_resource" "remote-exec-to-vpc2" {
  depends_on = [aws_instance.vpc1]
  triggers = {
        always_run = "${timestamp()}"
    }
  provisioner "remote-exec" {
    connection {
      agent       = false
      timeout     = "1m"
      host        = aws_instance.vpc2.public_ip
      user = "ec2-user"
      private_key = file(var.KEY_PAIR_PATH)
      type = "ssh"

    }

    inline = [
        "echo '${aws_instance.vpc1.private_ip}' > ~/ip.txt",
        "ping -c 5  ${aws_instance.vpc1.private_ip}  > /dev/null 2>&1  && echo 'OK' > ~/output.txt || echo 'NOK' > ~/output.txt",
    ]
  }
}


resource "null_resource" "remote-exec-to-vpc1" {
  depends_on = [aws_instance.vpc2]
  triggers = {
        always_run = "${timestamp()}"
    }
  
  provisioner "remote-exec" {
    connection {
      agent       = false
      timeout     = "1m"
      host        = aws_instance.vpc1.public_ip
      user = "ec2-user"
      private_key = file(var.KEY_PAIR_PATH)
      type = "ssh"

    }

      inline = [
        "echo '${aws_instance.vpc2.private_ip}' > ~/ip.txt",
        "ping -c 5  ${aws_instance.vpc2.private_ip}  > /dev/null 2>&1  && echo 'OK' > ~/output.txt || echo 'NOK' > ~/output.txt",
    ]
  }
}