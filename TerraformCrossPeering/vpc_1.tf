// CREATE VPC
resource "aws_vpc" "vpc1" {
  provider             = aws.vpc1
  cidr_block           = var.VPC_1_SUBNET
  enable_dns_hostnames = "true"
  enable_dns_support   = "true"

  tags = {
    Name = var.VPC_1_NAME
  }
}

// CREATE GATEWAY
resource "aws_internet_gateway" "vpc1" {
  provider = aws.vpc1
  vpc_id   = aws_vpc.vpc1.id

  tags = {
    Name = var.VPC_1_NAME
  }
}

// CREATE ROUTE TABLE
resource "aws_route_table" "vpc1" {
  provider = aws.vpc1
  vpc_id   = aws_vpc.vpc1.id
  route {
    cidr_block = "0.0.0.0/0"
    gateway_id = aws_internet_gateway.vpc1.id
  }

  route {
    cidr_block = var.VPC_2_SUBNET
    gateway_id = aws_vpc_peering_connection.vpc_peering.id
  }

  tags = {
    Name = var.VPC_1_NAME
  }
}

// CREATE SUBNET
resource "aws_subnet" "vpc1" {
  provider   = aws.vpc1
  vpc_id     = aws_vpc.vpc1.id
  cidr_block = var.VPC_1_SUBNET

  map_public_ip_on_launch = "true"

  tags = {
    Name = var.VPC_1_NAME
  }
}

resource "aws_route_table_association" "vpc1" {
  provider       = aws.vpc1
  subnet_id      = aws_subnet.vpc1.id
  route_table_id = aws_route_table.vpc1.id
}


// CREATE SECURITY GROUP
resource "aws_security_group" "vpc1" {
  provider = aws.vpc1
  vpc_id   = aws_vpc.vpc1.id

  ingress {
    from_port   = 22
    to_port     = 22
    protocol    = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
    description = "Allow input SSH"
  }

  ingress {
    from_port   = 0
    to_port     = 0
    protocol    = "-1"
    cidr_blocks = [var.VPC_2_SUBNET]
    description = "Allow all input traffic from other VPC"
  }

  egress {
    from_port   = 0
    to_port     = 0
    protocol    = "-1"
    cidr_blocks = ["0.0.0.0/0"]
    description = "Allow all ouput traffic from other VPC"
  }

  tags = {
    Name        = var.VPC_1_NAME
    Description = var.VPC_1_NAME
  }
}


// CREATE INSTANCE
resource "aws_instance" "vpc1" {
  provider = aws.vpc1


  ami                         = var.AMI_VPC_1
  instance_type               = var.INSTANCE_TYPE
  key_name                    = var.KEY_PAIR_NAME
  vpc_security_group_ids      = [aws_security_group.vpc1.id]
  subnet_id                   = aws_subnet.vpc1.id
  associate_public_ip_address = true
  source_dest_check           = false
  
  tags = {
    Name = var.VPC_1_NAME
  }
}



