provider "aws" {
  region = "us-east-1"
}

provider "aws" {
  alias  = "vpc1"
  region = var.VPC_1_REGION
}

provider "aws" {
  alias  = "vpc2"
  region = var.VPC_2_REGION
}
