# aws-vpc-peering

ping between 2 vpc using peering

task:
```
2 VPC
1 subnet on each VPC
1 instance on each subnet
1 script on each instance that doing 5 pings to the instance on second VPC (the ping should be to the private IP)
```


# Setup

1. create account to [amazone](https://aws.amazon.com/)
2. install aws cli [aws-cli](https://docs.aws.amazon.com/cli/latest/userguide/install-cliv2-linux.html) or [install_aws_cli_ubuntu](https://linuxhint.com/install_aws_cli_ubuntu/)

3. create user IAM User with policy deployement.
   > that will generate the  `ACCES KEY`  and `SECRET ACCESS KEY`
   > i have set policy `AdministratorAccess`
   
4. Set the `ACCES KEY`  and `SECRET ACCESS KEY` with this cmd `aws configure`:
   
   ouput 
   >i have already set that why you see ****
   ```bash
    AWS Access Key ID [****************O2C3]: 
    AWS Secret Access Key [****************nkkl]: 
    Default region name [eu-central-1]: 
    Default output format [json]: 
   ```
5. set key pair you can use via [aws-cli](https://docs.aws.amazon.com/cli/latest/userguide/cli-services-ec2-keypairs.html) or via [aws-console](https://docs.aws.amazon.com/AWSEC2/latest/UserGuide/ec2-key-pairs.html)
> the `key pair` are used for the deployement and ssh to the `vpc`

6. deployement under directory run
   ```bash
   # init
   terraform init
   # plan
   terraform plan
   # deploy
   terraform deploy

   ```
    via docker:
    ```
    # init
    docker  run --rm -it --user $(id -u):$(id -g) -e HOME=${HOME} -v "${HOME}:${HOME}:ro" -v "$(pwd):/usr/src/repo" -v "$(pwd)/..:/usr/src" -v /tmp:/tmp --workdir /usr/src/repo   hashicorp/terraform:latest "$@"  init

    # plan 
    docker  run --rm -it --user $(id -u):$(id -g) -e HOME=${HOME} -v "${HOME}:${HOME}:ro" -v "$(pwd):/usr/src/repo" -v "$(pwd)/..:/usr/src" -v /tmp:/tmp --workdir /usr/src/repo   hashicorp/terraform:latest "$@"  plan

    # deploy
    docker  run --rm -it --user $(id -u):$(id -g) -e HOME=${HOME} -v "${HOME}:${HOME}:ro" -v "$(pwd):/usr/src/repo" -v "$(pwd)/..:/usr/src" -v /tmp:/tmp --workdir /usr/src/repo   hashicorp/terraform:latest "$@"  deploy
    ```
    > that will promt to set some variable like `keypair.pem` **path and name**
    > use ` terrafom validate` cmd to check syntax code.

# Files

```
└── TerraformCrossPeering
    ├── main.tf
    ├── outputs.tf
    ├── peering.tf
    ├── test.tf
    ├── variables.tf
    ├── versions.tf
    ├── vpc_1.tf
    └── vpc_2.tf

```

- `main.tf` provider definition.  
- instance vpc files `vpc_1.tf`, `vpc_2.tf`.  (gateway, rouute table, subnet, security group etc ...).  
- `peering.tf` to define connection betwen 2 vpc instance.
- `test` run ping after the instace are created on each private ip vpc instance.


---

## Keyword

[VPC](https://docs.aws.amazon.com/vpc/latest/userguide/what-is-amazon-vpc.html "Virtual Private Cloud")

[EC2](https://docs.aws.amazon.com/AWSEC2/latest/UserGuide/concepts.html, "Amazon Elastic Compute")

[VPC peering](https://docs.aws.amazon.com/vpc/latest/peering/what-is-vpc-peering.html, "VPC peering")

[Terraform](https://www.terraform.io/ "Terraform site")
